package com.epam.dao;

import com.epam.exeption.MaximumDistanceExceededException;
import com.epam.model.Copter;
import com.epam.model.Position;

import java.util.List;

public interface CommonCopterDAO {
    boolean createCopter(Copter copter);

    List<Copter> getAllCopters();

    boolean deleteCopterById(int id);

    boolean changePositionById(int id, Position newPosition) throws MaximumDistanceExceededException;

    boolean goByDegree(int idCopter, double degree) throws MaximumDistanceExceededException;
}
